''' 
Django version: 2.0.1
Settings: https://docs.djangoproject.com/en/2.0/topics/settings/
Quick-start: https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/
Internationalization: https://docs.djangoproject.com/en/2.0/topics/i18n/
'''

import os
from decouple import config
from lib.UsefulFunctions.envUtils import get_env_settings

import dj_database_url

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))) # Extra os.path.dirname due to nested settings file

SECRET_KEY = config('SECRET_KEY')
ALLOWED_HOSTS = []

# Define required apps
PREREQ_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'whitenoise.runserver_nostatic', # Run whitenoise in development
    'django.contrib.staticfiles',
]

# Define project apps
PROJECT_APPS = [
    'user.apps.UsersConfig',
    'wakemeup.apps.WakemeupConfig',
    'crispy_forms',
    'django_tables2',
    'django.contrib.humanize',
    'widget_tweaks',
#    'svg',
]

# Create INSTALLED_APPS setting
INSTALLED_APPS = PREREQ_APPS + PROJECT_APPS

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'dcp.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates') # Template search directories
            ],
        'APP_DIRS': True, # Search for templates in app directories
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'dcp.contextprocessor.dcp'
            ],
        },
    },
]

WSGI_APPLICATION = 'dcp.wsgi.application'

# Hard-coded environment variable (used to generate DATABASES dictionary 'default')
DATABASE_URL = 'postgres://' + \
                config('DB_USER') + ':' + \
                config('DB_PASSWORD') + '@' + \
                config('DB_HOST') + ':' + \
                config('DB_PORT') + '/' + \
                config('DB_NAME') + \
                '?currentSchema=' + config('DB_SCHEMA_NAME')

# Use DATABASE_URL specified above if no environment variable provided
DATABASES = {'default' : dj_database_url.config(default=DATABASE_URL,conn_max_age=600, ssl_require=get_env_settings().get('ssl_require'))}

# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',},
]

# Internationalization
LANGUAGE_CODE = 'es-co'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATIC_URL = '/static/' # Url for static file serving
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static'),] # Search directories for static files (otherwise, engine only searches within app directories
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

# URL Redirects
LOGIN_REDIRECT_URL = '/' # Where to redirect login requests if "next" is not specified
LOGOUT_REDIRECT_URL = 'wakemeup:index' # Where to redirect login requests if "next" is not specified

# Authentication
AUTH_USER_MODEL = 'user.MyUser' # Custom user model
AUTHENTICATION_BACKENDS=['user.backends.MyBackend']
# MAX_UPLOAD_SIZE = 5242880 # Limit max file upload size for RestrictedFileField class
SESSION_EXPIRE_AT_BROWSER_CLOSE = True # Kill session on browser close

# E-mail config
EMAIL_HOST = config('EMAIL_HOST')
EMAIL_PORT = config('EMAIL_PORT')
EMAIL_HOST_USER = config('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = config('EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = config('EMAIL_USE_TLS')
DEFAULT_FROM_EMAIL = config('DEFAULT_FROM_EMAIL')

# Google Application
GOOGLE_APPLICATION_CREDENTIALS = config('GOOGLE_APPLICATION_CREDENTIALS')
GOOGLE_ADMIN_USER = config('GOOGLE_ADMIN_USER')

# Templates config
CRISPY_TEMPLATE_PACK = 'bootstrap4' # Set default template for forms
DJANGO_TABLES2_TEMPLATE = 'django_tables2/bootstrap-responsive.html' # Set default template for tables

# TO-DO: Check into these
DATETIME_FORMAT = 'j/n/y'
DATE_FORMAT = 'j/n/y'

# Numeric separator
USE_THOUSAND_SEPARATOR = True
THOUSAND_SEPARATOR = '.'

SHORT_DATETIME_FORMAT = 'j/n/y' # Used for django-tables2

# Ignore errors on start-up
SILENCED_SYSTEM_CHECKS = ['models.E005']

# Custom settings
DEFAULT_SCHOOL_YEAR = config('DEFAULT_SCHOOL_YEAR') # Default school year to display