from django.apps import AppConfig


class WakemeupConfig(AppConfig):
    name = 'wakemeup'
